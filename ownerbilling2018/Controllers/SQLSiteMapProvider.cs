﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Security.Permissions;
using System.Web;
using System.Web.Configuration;
using MvcSiteMapProvider;


namespace ownerbilling2018.Controllers
{

    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class StoreDetailsDynamicNodeProvider : DynamicNodeProviderBase
    {
        private MvcSiteMapProvider.DynamicNode aRootNode = null;
        private bool initialized = false;
        private Guid oAUID;
        private Guid oAPID;
        private string oCurrentURL;

        public StoreDetailsDynamicNodeProvider()
        {
        }

        public StoreDetailsDynamicNodeProvider(Guid AUID, string CurrentURL)
        {
            UserID = AUID;
            oCurrentURL = CurrentURL;
        }

        public bool IsInitialized
        {
            get
            {
                return initialized;
            }
        }

        private Guid UserID
        {
            set
            {
                oAUID = value;
            }
        }

        private Guid ApplicationID
        {
            set
            {
                oAPID = value;
            }
        }



        public override IEnumerable<MvcSiteMapProvider.DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            lock (this)
            {

                const string ConnString = "KPI_Security";
                const string SqlCmd = "KPISecurity2011.ApplicationMenu_List";
                SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
                SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);
                SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter();
                DataSet oDataSet = new DataSet();
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);


                oSqlCommand.Parameters["@AUID"].Value = HttpContext.Current.Session["KPISystemUserID"];
                //oSqlCommand.Parameters["@AUID"].Value = new Guid("21BC5383-7B69-4660-AC35-92DE5B576CC6");

                string mypath = HttpContext.Current.Request.Url.PathAndQuery;
                oSqlCommand.Parameters["@NodeAddress"].Value = mypath;
                // oSqlCommand.Parameters["@NodeAddress"].Value = "/Application/CCG/KPI/Indicator2015.aspx?AppID=CCG";

                oSqlDataAdapter.SelectCommand = oSqlCommand;
                oSqlDataAdapter.Fill(oDataSet, "ApplicationMenu_List");
                oDataSet.Relations.Add("GroupRelation", oDataSet.Tables[0].Columns["NodeID"], oDataSet.Tables[0].Columns["ParentID"], false);
                string companylogo = "";
                string pagetitle = "";

                foreach (DataRow ParentRow in oDataSet.Tables[0].Rows)
                {

                    if (ParentRow["ParentID"] != DBNull.Value)
                    {
                        MvcSiteMapProvider.DynamicNode ChildNode = new MvcSiteMapProvider.DynamicNode(ParentRow["NodeID"].ToString(), ParentRow["NodeDescription"].ToString());
                        ChildNode.Url = CSInclude.ReplaceDBNull(ParentRow["NodeAddress"], "").ToString();
                        if (ParentRow["ParentID"].ToString() != "00000000-0000-0000-0000-000000000000")
                        {
                            ChildNode.ParentKey = ParentRow["ParentID"].ToString(); ;
                        }

                        if (ParentRow["NodeAddress"] != DBNull.Value)
                        {
                            if (pagetitle == "")
                            {
                                pagetitle = ParentRow["NodeDescription"].ToString();
                                HttpContext.Current.Session["PageTitle"] = pagetitle;
                            }
                            if (ParentRow["NodeLogoPath"] != DBNull.Value)
                            {
                                companylogo = ParentRow["NodeLogoPath"].ToString();
                                HttpContext.Current.Session["CompanyLogo"] = companylogo;
                            }
                            if (HttpContext.Current.Session["CompanyLogo"] == null)
                            {
                                HttpContext.Current.Session["CompanyLogo"] = "";
                            }
                            try
                            {
                                if (ParentRow["NodeAddress"].ToString().Substring(0, 9) == "/WebLink/")
                                {
                                    ChildNode.Url = ParentRow["NodeAddress"].ToString().Replace("/WebLink/", "https://");
                                }
                                if (ParentRow["NodeAddress"].ToString().Substring(0, 10) == "/WebLinks/")
                                {
                                    ChildNode.Url = ParentRow["NodeAddress"].ToString().Replace("/WebLinks/", "https://");
                                }
                            }
                            catch (Exception ex)
                            {

                            }

                        }
                        yield return ChildNode;
                    }

                }
                oDataSet.Dispose();
                oSqlDataAdapter.Dispose();
                oSqlCommand.Dispose();
                oSqlConnection.Close();


            }
        }


    }


}