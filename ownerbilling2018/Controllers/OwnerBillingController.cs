﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Data.SqlClient;
using System.Configuration;
using PagedList;
using PagedList.Mvc;
using System.IO;
using System.Net.Http;
using System.Web.Security;
using ownerbilling2018.Models;

namespace ownerbilling2018.Controllers
{
    public class OwnerBillingController : KPIController
    {
        private App_KPI_OwnerBillingEntities db = new App_KPI_OwnerBillingEntities();
        private App_KPI_OwnerBillingEntitiesRegion dbRegion = new App_KPI_OwnerBillingEntitiesRegion();
        private App_KPI_OwnerBillingEntitiesCompany dbCompany = new App_KPI_OwnerBillingEntitiesCompany();

        [HttpPost]
        [Authorize]
        public ActionResult Edit([Bind(Include = "OBSubWorkEstimate,OBIsCurrent,jobdescription,MMMCU,OBSeqNoId,OBApplicationNo,OBRequisitionDate,OBStatus,OBStatusDescription,OBSubmitDate,OBApproveDate,OBTotalEstimate,OBTotalFinal,KDOwnerPaymentReceivedDate,G4DueDate,OBUserName,OBDateAdd,OBDateChange,OBClarkWorkEstimate,OBOwnerRetentionEstimate, OBSubRetentionEstimate, OBSubWorkFinal, OBClarkWorkFinal, OBOwnerRetentionFinal, OBSubRetentionFinal, OBRemark")] spOwnerBilling_Result OB, string referrer, string buttontype)
        {
            if (TempData.Peek("KeyType") == null)
            {
                string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
                string ConnString = "KPI_Security";
                string SqlCmd = "KPISecurity2018.Application_CheckAccess";
                SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
                SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

                try
                {
                    oSqlConnection.Open();
                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(oSqlCommand);
                    oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                    //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                    oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                    //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                    oSqlCommand.ExecuteNonQuery();

                    TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                    TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                    TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                    TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                    //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                    TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                    TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                    oSqlCommand.Dispose();
                    oSqlConnection.Close();
                    Session["OBLoggedIn"] = true;

                    switch (Convert.ToInt16(TempData.Peek("KeyType")))
                    {
                        case 1010:
                            TempData["LevelMin"] = 50;
                            break;
                        case 1020:
                            TempData["LevelMin"] = 10;
                            break;
                        case 1030:
                            TempData["LevelMin"] = 10;
                            break;
                        case 1060:
                            TempData["LevelMin"] = 10;
                            break;
                        case 6010:
                            TempData["LevelMin"] = 10;
                            break;
                        default:
                            TempData["LevelMin"] = 10;
                            break;
                    }

                }
                catch (Exception ex)
                {
                    oSqlCommand.Dispose();
                    oSqlConnection.Close();
                    ViewBag.Error = ex.Message;
                    return View();
                    
                }
            }
            Guid myid = Guid.Parse(Session["KPISystemUserID"].ToString());
            short myshort = short.Parse(TempData.Peek("KeyType").ToString());
            if (referrer == "Index2" || referrer == "Details")
            {

                spOwnerBilling_Result r1 = db.spOwnerBilling(myshort, myid, OB.MMMCU, OB.OBSeqNoId).First();

                return View(r1);

            }
            else
            {



                dynamic showMessageString = string.Empty;


                switch (buttontype)
                {
                    case "Cancel":
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "Cancelled"

                        };
                        break;
                    case "Save":
                    case "Submit Pencil Draft":
                    case "Submit Final":
                        using (var con = new SqlConnection

                    (ConfigurationManager.ConnectionStrings["App_OwnerBilling"].ConnectionString))

                        {

                            var cmd = new SqlCommand("OwnerBilling2017.OwnerBilling", con);

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(new SqlParameter("@OBMCU", SqlDbType.VarChar)).Value = OB.MMMCU;

                            cmd.Parameters.Add(new SqlParameter("@OBSeqNo", SqlDbType.SmallInt)).Value = OB.OBSeqNoId;

                            cmd.Parameters.Add(new SqlParameter("@OBApplicationNo", SqlDbType.SmallInt)).Value = OB.OBApplicationNo;

                            if (buttontype == "Submit Pencil Draft")
                            {
                                cmd.Parameters.Add(new SqlParameter("@OBStatus", SqlDbType.TinyInt)).Value = 10;
                            }
                            else if (buttontype == "Submit Final")
                            {
                                cmd.Parameters.Add(new SqlParameter("@OBStatus", SqlDbType.TinyInt)).Value = 20;
                            }
                            else
                            {
                                cmd.Parameters.Add(new SqlParameter("@OBStatus", SqlDbType.TinyInt)).Value = OB.OBStatus;
                            }


                            cmd.Parameters.Add(new SqlParameter("@OBSubmitDate", SqlDbType.Date)).Value = OB.OBSubmitDate;

                            cmd.Parameters.Add(new SqlParameter("@OBApproveDate", SqlDbType.Date)).Value = OB.OBApproveDate;

                            cmd.Parameters.Add(new SqlParameter("@OBSubWorkEstimate", SqlDbType.Money)).Value = OB.OBSubWorkEstimate;

                            cmd.Parameters.Add(new SqlParameter("@OBClarkWorkEstimate ", SqlDbType.Money)).Value = OB.OBClarkWorkEstimate;

                            cmd.Parameters.Add(new SqlParameter("@OBOwnerRetentionEstimate", SqlDbType.Money)).Value = OB.OBOwnerRetentionEstimate;

                            cmd.Parameters.Add(new SqlParameter("@OBSubRetentionEstimate", SqlDbType.Money)).Value = OB.OBSubRetentionEstimate;

                            cmd.Parameters.Add(new SqlParameter("@OBSubWorkFinal", SqlDbType.Money)).Value = OB.OBSubWorkFinal;

                            cmd.Parameters.Add(new SqlParameter("@OBClarkWorkFinal", SqlDbType.Money)).Value = OB.OBClarkWorkFinal;

                            cmd.Parameters.Add(new SqlParameter("@OBOwnerRetentionFinal", SqlDbType.Money)).Value = OB.OBOwnerRetentionFinal;

                            cmd.Parameters.Add(new SqlParameter("@OBSubRetentionFinal", SqlDbType.Money)).Value = OB.OBSubRetentionFinal;

                            cmd.Parameters.Add(new SqlParameter("@OBRemark", SqlDbType.VarChar)).Value = OB.OBRemark;

                            cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                            cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 70;

                            try

                            {

                                if (con.State != ConnectionState.Open)

                                    con.Open();

                                cmd.ExecuteNonQuery();
                                showMessageString = new
                                {
                                    param1 = 200,
                                    param2 = "success"

                                };

                            }
                            catch (Exception ex)
                            {
                                showMessageString = new
                                {
                                    param1 = 404,
                                    param2 = ex.Message
                                };

                            }

                            finally

                            {

                                if (con.State != ConnectionState.Closed)

                                    con.Close();

                            }

                        }
                        return Json(showMessageString, JsonRequestBehavior.AllowGet);
                    //break;


                    case "Skip This Month":
                        using (var con = new SqlConnection

                (ConfigurationManager.ConnectionStrings["App_OwnerBilling"].ConnectionString))

                        {

                            var cmd = new SqlCommand("OwnerBilling2017.OwnerBilling", con);

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(new SqlParameter("@OBMCU", SqlDbType.VarChar)).Value = OB.MMMCU;

                            cmd.Parameters.Add(new SqlParameter("@OBSeqNo", SqlDbType.SmallInt)).Value = OB.OBSeqNoId;

                            cmd.Parameters.Add(new SqlParameter("@OBApplicationNo", SqlDbType.SmallInt)).Value = OB.OBApplicationNo;

                            cmd.Parameters.Add(new SqlParameter("@OBStatus", SqlDbType.TinyInt)).Value = 30;

                            cmd.Parameters.Add(new SqlParameter("@OBRemark", SqlDbType.VarChar)).Value = OB.OBRemark;

                            cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                            cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 71;

                            try

                            {

                                if (con.State != ConnectionState.Open)

                                    con.Open();

                                cmd.ExecuteNonQuery();

                                showMessageString = new
                                {
                                    param1 = 200,
                                    param2 = "Skipped"

                                };



                            }
                            catch (Exception ex)
                            {
                                showMessageString = new
                                {
                                    param1 = 404,
                                    param2 = ex.Message
                                };

                            }


                            finally

                            {

                                if (con.State != ConnectionState.Closed)

                                    con.Close();

                            }

                        }

                        break;

                }

                spOwnerBilling_Result r1 = db.spOwnerBilling(myshort, myid, OB.MMMCU, OB.OBSeqNoId).First();

                return View(r1);

            }

        }

        [HttpGet]
        public ActionResult NoAccess(string MsgID)
        {
            ViewBag.Error = MsgID;
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Default(string myaction, string searchString, string Regions, string Companies, string sortOrder, string currentFilter, string currentFilterR, string currentFilterC, string MyPagedList, string whichpage, string whichpagenum)
        {

            /***********************************************************************************************/
            /* This code should only execute the first time the page is loaded, hence why it is in the GET */
            /***********************************************************************************************/

            string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISecurity2018.Application_CheckAccess";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

            try
            {
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);
                oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                oSqlCommand.ExecuteNonQuery();

                TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                oSqlCommand.Dispose();
                oSqlConnection.Close();
                Session["OBLoggedIn"] = true;

                switch (Convert.ToInt16(TempData.Peek("KeyType")))
                {
                    case 1010:
                        TempData["LevelMin"] = 50;
                        break;
                    case 1020:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1030:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1060:
                        TempData["LevelMin"] = 10;
                        break;
                    case 6010:
                        TempData["LevelMin"] = 10;
                        break;
                    default:
                        TempData["LevelMin"] = 10;
                        break;
                }

            }
            catch (Exception ex)
            {
                oSqlCommand.Dispose();
                oSqlConnection.Close();
                ViewBag.Error = ex.Message;
                //List<spOwnerBilling_Result> requisitions = db.spOwnerBilling(myshort, myid, null, null).ToList();
                return View(new List<spOwnerBilling_Result>());
            }

            /*******************************************************************/
            /*******************************************************************/
            /*******************************************************************/

            if (myaction == null || myaction == "Default")
            {
                if (String.IsNullOrEmpty(searchString))
                {
                    searchString = currentFilter;
                }
                if (String.IsNullOrEmpty(Regions))
                {
                    Regions = currentFilterR;
                }
                if (String.IsNullOrEmpty(Companies))
                {
                    Companies = currentFilterC;
                }

                ViewBag.CurrentSort = sortOrder;
                ViewBag.CurrentFilter = searchString;
                ViewBag.CurrentFilterR = Regions;
                ViewBag.CurrentFilterC = Companies;
                Guid myid = Guid.Parse(Session["KPISystemUserID"].ToString());
                short myshort = short.Parse(TempData.Peek("KeyType").ToString());

                var myregions = new SelectList(dbRegion.spOwnerBillingRegion(myshort, myid).Select(r => r.Entity).ToList(), Regions);

                ViewBag.Regions = myregions;


                var companies = new SelectList(dbCompany.spOwnerBillingCompany(myshort, myid).Select(r => r.ENDescription).ToList(), Companies);

                ViewBag.Companies = companies;
                ViewBag.Companycount = companies.Count();

                //SEARCH FILTERS

                List<spOwnerBilling_Result> requisitions = db.spOwnerBilling(myshort, myid, null, null).ToList();

                int numreq = requisitions.Count();

                // SORTING

                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "jobdescrption_desc" : "";
                ViewBag.DateSortParm = sortOrder == "OBRequisitionDate" ? "OBRequisitionDate_desc" : "OBRequisitionDate";
                ViewBag.AppSortParm = sortOrder == "OBApplicationNo" ? "OBApplicationNo_desc" : "OBApplicationNo";

                // PAGING STUFF

                int pageSize = 25;
                int pageNumber = 1;
                decimal mypgct = numreq / Convert.ToDecimal(pageSize);
                int mypagecount = Convert.ToInt32(Math.Ceiling(mypgct));
                ViewBag.mypagecount = mypagecount;
                if (MyPagedList != "" && MyPagedList != null)
                {
                    pageNumber = Convert.ToInt32(MyPagedList);
                }
                else if (whichpagenum != "" && whichpagenum != null)
                {
                    pageNumber = Convert.ToInt32(whichpagenum);
                }

                if (pageNumber > mypagecount)
                {
                    pageNumber = 1;
                }

                switch (whichpage)
                {
                    case "Next":
                        pageNumber++;
                        break;
                    case "Last":
                        pageNumber = mypagecount;
                        break;
                    case "Previous":
                        pageNumber--;
                        break;
                    case "First":
                        pageNumber = 1;
                        break;
                }

                var list = new List<SelectListItem>();

                for (int i = 1; i <= mypagecount; i++)
                    list.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });

                ViewBag.list = list;
                ViewBag.pagenum = pageNumber.ToString();

                return View(requisitions.ToPagedList(pageNumber, pageSize));

            }
            else
            {
                return View();
            }


        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Default(string myaction, string MMMCU, short? OBSeqNoId, DateTime? OBRequisitionDate, string referrer, string buttontype, string searchString, string Regions, string Companies, string sortOrder, string currentFilter, string currentFilterR, string currentFilterC, string MyPagedList, string whichpage, string whichpagenum)
        {
            if (TempData.Peek("KeyType") == null)
            {
                string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
                string ConnString = "KPI_Security";
                string SqlCmd = "KPISecurity2018.Application_CheckAccess";
                SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
                SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

                try
                {
                    oSqlConnection.Open();
                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(oSqlCommand);
                    oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                    //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                    oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                    //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                    oSqlCommand.ExecuteNonQuery();

                    TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                    TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                    TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                    TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                    //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                    TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                    TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                    oSqlCommand.Dispose();
                    oSqlConnection.Close();
                    Session["OBLoggedIn"] = true;

                    switch (Convert.ToInt16(TempData.Peek("KeyType")))
                    {
                        case 1010:
                            TempData["LevelMin"] = 50;
                            break;
                        case 1020:
                            TempData["LevelMin"] = 10;
                            break;
                        case 1030:
                            TempData["LevelMin"] = 10;
                            break;
                        case 1060:
                            TempData["LevelMin"] = 10;
                            break;
                        case 6010:
                            TempData["LevelMin"] = 10;
                            break;
                        default:
                            TempData["LevelMin"] = 10;
                            break;
                    }

                }
                catch (Exception ex)
                {
                    oSqlCommand.Dispose();
                    oSqlConnection.Close();
                    ViewBag.Error = ex.Message;
                    return View();
                }
            }
            Guid myid = Guid.Parse(Session["KPISystemUserID"].ToString());
            short myshort = short.Parse(TempData.Peek("KeyType").ToString());
            if (myaction == "Default")
            {
                if (String.IsNullOrEmpty(searchString))
                {
                    searchString = currentFilter;
                }
                if (String.IsNullOrEmpty(Regions))
                {
                    Regions = currentFilterR;
                }
                if (String.IsNullOrEmpty(Companies))
                {
                    Companies = currentFilterC;
                }

                ViewBag.CurrentSort = sortOrder;
                ViewBag.CurrentFilter = searchString;
                ViewBag.CurrentFilterR = Regions;
                ViewBag.CurrentFilterC = Companies;


                var myregions = new SelectList(dbRegion.spOwnerBillingRegion(myshort, myid).Select(r => r.Entity).ToList(), Regions);

                ViewBag.Regions = myregions;

                var companies = new SelectList(dbCompany.spOwnerBillingCompany(myshort, myid).Select(r => r.ENDescription).ToList(), Companies);

                ViewBag.Companies = companies;
                ViewBag.Companycount = companies.Count();

                //SEARCH FILTERS

                //var requisitions = from s in db.spOwnerBilling(myshort, myid, null, null) select s;
                IEnumerable<spOwnerBilling_Result> requisitions = db.spOwnerBilling(myshort, myid, null, null).AsQueryable().ToList();

                if (!String.IsNullOrEmpty(searchString))
                {
                    requisitions = requisitions.Where(s => s.jobdescription.ToLower().Contains(searchString.Trim().ToLower()));
                }
                if (!String.IsNullOrEmpty(Regions))
                {
                    requisitions = requisitions.Where(s => s.MMRegion.Contains(Regions) || (s.MMRegionGroup != null && s.MMRegionGroup.Contains(Regions)));
                }
                if (!String.IsNullOrEmpty(Companies))
                {
                    requisitions = requisitions.Where(s => s.ENDescription.Contains(Companies));
                }

                int numreq = requisitions.Count();

                // SORTING

                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "jobdescrption_desc" : "";
                ViewBag.DateSortParm = sortOrder == "OBRequisitionDate" ? "OBRequisitionDate_desc" : "OBRequisitionDate";
                ViewBag.AppSortParm = sortOrder == "OBApplicationNo" ? "OBApplicationNo_desc" : "OBApplicationNo";
                switch (sortOrder)
                {
                    case "jobdescrption_desc":
                        requisitions = requisitions.OrderByDescending(s => s.jobdescription);
                        break;
                    case "OBRequisitionDate":
                        requisitions = requisitions.OrderBy(s => s.OBRequisitionDate);
                        break;
                    case "OBRequisitionDate_desc":
                        requisitions = requisitions.OrderByDescending(s => s.OBRequisitionDate);
                        break;
                    case "OBApplicationNo":
                        requisitions = requisitions.OrderBy(s => s.OBApplicationNo);
                        break;
                    case "OBApplicationNo_desc":
                        requisitions = requisitions.OrderByDescending(s => s.OBApplicationNo);
                        break;
                    default:
                        requisitions = requisitions.OrderBy(s => s.jobdescription);
                        break;
                }

                // PAGING STUFF

                int pageSize = 25;
                int pageNumber = 1;
                decimal mypgct = numreq / Convert.ToDecimal(pageSize);
                int mypagecount = Convert.ToInt32(Math.Ceiling(mypgct));
                ViewBag.mypagecount = mypagecount;
                if (MyPagedList != "" && MyPagedList != null)
                {
                    pageNumber = Convert.ToInt32(MyPagedList);
                }
                else if (whichpagenum != "" && whichpagenum != null)
                {
                    pageNumber = Convert.ToInt32(whichpagenum);
                }

                if (pageNumber > mypagecount)
                {
                    pageNumber = 1;
                }

                switch (whichpage)
                {
                    case "Next":
                        pageNumber++;
                        break;
                    case "Last":
                        pageNumber = mypagecount;
                        break;
                    case "Previous":
                        pageNumber--;
                        break;
                    case "First":
                        pageNumber = 1;
                        break;
                }

                var list = new List<SelectListItem>();

                for (int i = 1; i <= mypagecount; i++)
                    list.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });

                ViewBag.list = list;
                ViewBag.pagenum = pageNumber.ToString();

                return View(requisitions.ToPagedList(pageNumber, pageSize));
                // return View(requisitions);
            }
            else if (myaction == "Index2")
            {
                while (MMMCU.Length < 12)
                {
                    MMMCU = " " + MMMCU;

                }

                List<spOwnerBilling_Result> r1 = db.spOwnerBilling(myshort, myid, MMMCU, null).ToList();

                return View(r1);
            }
            else if (myaction == "Details")
            {
                if (MMMCU == null || OBSeqNoId == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    while (MMMCU.Length < 12)
                    {
                        MMMCU = " " + MMMCU;
                    }
                    spOwnerBilling_Result OB2 = db.spOwnerBilling(myshort, myid, MMMCU, OBSeqNoId).First();
                    if (OB2 == null)
                    {
                        return HttpNotFound();
                    }
                    return View(OB2);
                }
            }
            else if (myaction == "Delete")
            {
                dynamic showMessageString = string.Empty;


                using (var con = new SqlConnection

                (ConfigurationManager.ConnectionStrings["App_OwnerBilling"].ConnectionString))

                {

                    var cmd = new SqlCommand("OwnerBilling2017.OwnerBilling", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@OBMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@OBSeqNo", SqlDbType.SmallInt)).Value = OBSeqNoId;

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 80;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "row-" + OBSeqNoId

                        };


                    }
                    catch (Exception ex)
                    {
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };
                    }

                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }

                }

                if (referrer == "Details")
                {
                    List<spOwnerBilling_Result> r1 = db.spOwnerBilling(myshort, myid, MMMCU, null).ToList();

                    return View(r1);

                }
                else
                {
                    return Json(showMessageString, JsonRequestBehavior.AllowGet);
                }


            }
            else if (myaction == "CreateNew")
            {
                short result = 0;
                ViewBag.Message = null;

                dynamic showMessageString = string.Empty;
                using (var con = new SqlConnection

                    (ConfigurationManager.ConnectionStrings["App_OwnerBilling"].ConnectionString))

                {

                    var cmd = new SqlCommand("OwnerBilling2017.OwnerBilling", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@OBMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@OBRequisitionDate", SqlDbType.Date)).Value = OBRequisitionDate;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 60;

                    var returnParameter = cmd.Parameters.Add("@ReturnVal", SqlDbType.Int);

                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();

                        result = Convert.ToInt16(returnParameter.Value);
                        short? newOBSeqNoId = result;
                        showMessageString = new

                        {
                            param1 = 200,
                            param2 = newOBSeqNoId
                        };

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message;
                        var errorMsg = ex.Message.ToString();
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };
                    }


                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }


                }

                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            else if (myaction == "Edit")
            {
                spOwnerBilling_Result r1 = db.spOwnerBilling(myshort, myid, MMMCU, OBSeqNoId).First();

                return View(r1);

            }
            else if (myaction == "ReopenPending")
            {
                dynamic showMessageString = string.Empty;

                using (var con = new SqlConnection

                    (ConfigurationManager.ConnectionStrings["App_OwnerBilling"].ConnectionString))

                {

                    var cmd = new SqlCommand("OwnerBilling2017.OwnerBilling", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@OBMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@OBSeqNo", SqlDbType.SmallInt)).Value = OBSeqNoId;

                    cmd.Parameters.Add(new SqlParameter("@OBStatus", SqlDbType.TinyInt)).Value = 0;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 72;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "Pending"

                        };
                    }
                    catch (Exception ex)
                    {
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };

                    }

                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }

                }


                spOwnerBilling_Result r1 = db.spOwnerBilling(myshort, myid, MMMCU, OBSeqNoId).First();

                return View(r1);

            }
            else if (myaction == "PencilMonth")
            {
                dynamic showMessageString = string.Empty;

                using (var con = new SqlConnection

                    (ConfigurationManager.ConnectionStrings["App_OwnerBilling"].ConnectionString))

                {

                    var cmd = new SqlCommand("OwnerBilling2017.OwnerBilling", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@OBMCU", SqlDbType.VarChar)).Value = MMMCU;

                    cmd.Parameters.Add(new SqlParameter("@OBSeqNo", SqlDbType.SmallInt)).Value = OBSeqNoId;

                    cmd.Parameters.Add(new SqlParameter("@OBStatus", SqlDbType.TinyInt)).Value = 10;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar, 100)).Value = Session["KPISystemUserName"].ToString();

                    cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int)).Value = 72;

                    try

                    {

                        if (con.State != ConnectionState.Open)

                            con.Open();

                        cmd.ExecuteNonQuery();
                        showMessageString = new
                        {
                            param1 = 200,
                            param2 = "Pencil Submitted"

                        };

                    }
                    catch (Exception ex)
                    {
                        showMessageString = new
                        {
                            param1 = 404,
                            param2 = ex.Message
                        };

                    }

                    finally

                    {

                        if (con.State != ConnectionState.Closed)

                            con.Close();

                    }

                }

                spOwnerBilling_Result r1 = db.spOwnerBilling(myshort, myid, MMMCU, OBSeqNoId).First();

                return View(r1);

            }
            else
            {
                return View();
            }
        }

        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            if (CSInclude.SSO_Check_LocalIP(Request.ServerVariables["REMOTE_ADDR"].ToString()) != 0)
            {
                if (Request.QueryString["ReturnUrl"] != null || Request.QueryString["ReturnID"] != null)
                {
                    if (Request.QueryString["ReturnID"] == null)
                    {
                        string SingleSignOnURL = System.Configuration.ConfigurationManager.AppSettings["KPISingleSignOnURL"].ToString();
                        string ReturnID = CSInclude.SSO_Create_Ticket(Request.ServerVariables["REMOTE_ADDR"].ToString(), CSInclude.Build_ApplicationPath(true)).ToString();
                        Response.Redirect(SingleSignOnURL + "?ReturnID=" + ReturnID + "&ReturnPath=" + CSInclude.Build_ApplicationPath(false));
                        Response.End();
                    }
                    else
                    {
                        string RtnKey = CSInclude.SSO_Delete_Ticket(Request.QueryString["ReturnID"]).ToString();
                        if (RtnKey != "")
                        {

                            FormsAuthentication.RedirectFromLoginPage(RtnKey, false);



                            Response.End();
                        }
                    }
                }
            }

            // If server variables are missing or there is no "ReturnURL" or "ReturnID" in the querystring, then something is wrong.
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}