﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Configuration;

namespace ownerbilling2018.Controllers
{
    public class KpiSecurity
    {
        private Guid pUserID;
        private string pFullName;
        private int pCeridianFlxID;
        private string pEmailAddress;
        private string pIsOfficer;
        private Hashtable pApplication = new Hashtable();
        private CSInclude.AuthApplication pDefaultApplication;
        private Int32 pKPI2001ID;

        public KpiSecurity(string UserName, string UserNameAlt)
        {
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISystem.DirectoryUser_Get";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);
            oSqlConnection.Open();
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(oSqlCommand);
            oSqlCommand.Parameters["@Key"].Value = CSInclude.ReplaceDBNullOrEmpty(UserNameAlt, UserName);
            SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader();

            if (oSqlDataReader.HasRows)
            {
                oSqlDataReader.Read();
                pUserID = new Guid(oSqlDataReader["KYID"].ToString());
                pFullName = oSqlDataReader["KYFullName"].ToString();
                pCeridianFlxID = (int)CSInclude.ReplaceDBNull(oSqlDataReader["KYFlxID"], 0);
                pEmailAddress = (string)CSInclude.ReplaceDBNull(oSqlDataReader["KYEmailAddress"], "");
                pKPI2001ID = (int)CSInclude.ReplaceDBNull(oSqlDataReader["KYKPI2001ID"], 0);
                pIsOfficer = (string)CSInclude.ReplaceDBNull(oSqlDataReader["RWOfficer"], "N");
                LoadAuthApplication();
            }

            
        }

        private void LoadAuthApplication()
        {
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISystem.Application_List";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);
            oSqlConnection.Open();
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(oSqlCommand);
            oSqlCommand.Parameters["@UserID"].Value = pUserID;
            SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader();

            if (oSqlDataReader.HasRows)
            {
                while (oSqlDataReader.Read()) 
                { 
                    CSInclude.AuthApplication App;
                    App.ApplicationID = Guid.Parse(oSqlDataReader["APID"].ToString());
                    App.ApplicationName = oSqlDataReader["APName"].ToString();
                    App.ApplicationDefaultFolder = oSqlDataReader["APDefaultFolder"].ToString();
                    App.ApplicationDefaultPage = oSqlDataReader["APDefaultPage"].ToString();
                    App.ApplicationLogoPath = (string)CSInclude.ReplaceDBNull(oSqlDataReader["APLogoPath"], "");
                    App.ApplicationHasSecurityCode = (bool)oSqlDataReader["APHasSecurityCode"];
                    App.ApplicationHasUserSecurity = (bool)oSqlDataReader["APHasUserSecurity"];
                    App.UserAccessLevel = Convert.ToInt16(oSqlDataReader["SELevel"]);
                    App.UserIsAdmin = (oSqlDataReader["SEAdmin"] as int? == 1) ? true : false;
                    App.UserFlag1 = (oSqlDataReader["SEFlag1"] as int? == 1) ? true : false;
                    App.UserSecurityCode = oSqlDataReader["SESecurityCode"].ToString();
                    string appfolder = oSqlDataReader["APDefaultFolder"].ToString();
                    pApplication.Add(appfolder, App);
                    if (pApplication.Count == 1)
                    {
                        pDefaultApplication = App;
                    }
                }
            }

            oSqlDataReader.Close();
            oSqlCommand.Dispose();
            oSqlConnection.Close();
           
        }

        public Guid UserID
        {
            get
            {
                return pUserID;
            }
        }
        public string FullName
        {
            get
            {
                return pFullName;
            }
        }
        public string CeridianFlxID
        {
            get
            {
                return pCeridianFlxID.ToString();
            }
        }
        public string EmailAddress
        {
            get
            {
                return pEmailAddress;
            }
        }
        public Int32 KPI2001ID
        {
            get
            {
                return pKPI2001ID;
            }
        }
        public Hashtable Applications
        {
            get
            {
                return pApplication;
            }
        }
        public CSInclude.AuthApplication ApplicationDefault
        {
            get
            {
                return pDefaultApplication;
            }
        }

        

    }
}