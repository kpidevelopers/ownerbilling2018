﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using System.Web.Configuration;


namespace ownerbilling2018.Controllers
{
    public static class CSInclude
    {
        public struct AuthApplication
        {
            public Guid ApplicationID;
            public string ApplicationName;
            public string ApplicationDefaultFolder;
            public string ApplicationDefaultPage;
            public string ApplicationLogoPath;
            public bool ApplicationHasSecurityCode;
            public bool ApplicationHasUserSecurity;
            public Int16 UserAccessLevel;
            public bool UserIsAdmin;
            public bool UserFlag1;
            public string UserSecurityCode;
            public string ApplicationDefaultPath()
            {
                return ApplicationDefaultFolder + ApplicationDefaultPage;
            }
        }

        
        public static Object ReplaceDBNull(Object OriginalValue, Object DefaultValue)
        {
            return (OriginalValue == DBNull.Value) ? DefaultValue : OriginalValue;
        }

        public static Object ReplaceDBNullOrEmpty(Object OriginalValue, Object DefaultValue)
        {
            return (OriginalValue == null || (string)OriginalValue == String.Empty) ? DefaultValue : OriginalValue;
        }

        public static Object ReplaceNothing(Object OriginalValue, Object DefaultValue)
        {
            return (OriginalValue == null) ? DefaultValue : OriginalValue;
        }

        public static Object ReplaceDefaultToEmpty(Object OriginalValue, Object DefaultValue)
        {
            return (OriginalValue == DefaultValue) ? "" : OriginalValue;
        }

        public static int EncryptKeyTypeCode(int KeyTypeCode)
        {
            DateTime TempDate = DateTime.Now;
            int TempResult;
            TempResult = KeyTypeCode + TempDate.Year;
            TempResult *= TempDate.Month;
            TempResult -= TempDate.Day;
            return TempResult;
        }

        public static string EncryptKPI2001ID(int KPI2001ID)
        {
            string TempString = Guid.NewGuid().ToString();
            string TempID = ("0000" + KPI2001ID).Substring(("0000" + KPI2001ID).Length - 4);
            TempString = TempString.Remove(10, 1);
            TempString = TempString.Insert(10, TempID.Substring(1, 1));
            TempString = TempString.Remove(15, 1);
            TempString = TempString.Insert(15, TempID.Substring(2, 1));
            TempString = TempString.Remove(20, 1);
            TempString = TempString.Insert(20, TempID.Substring(3, 1));
            TempString = TempString.Remove(25, 1);
            TempString = TempString.Insert(25, TempID.Substring(4, 1));
            return TempString;
        }

        public static int ApplicationIndex(ArrayList AppPath, string CurrentPage)
        {
                string CurrentPath = CurrentPage.Substring(0, CurrentPage.LastIndexOf("/") + 1);
                return AppPath.IndexOf(CurrentPath);
         }

        public static AuthApplication? ApplicationCurrent(Hashtable Applications,string CurrentPagePath)
        {
            string CurrentPageFolder;
            if (CurrentPagePath.LastIndexOf("/") == 0)
            {
                CurrentPageFolder = CurrentPagePath.ToLower();
            } else
            {
                CurrentPageFolder = CurrentPagePath.Substring(0, CurrentPagePath.LastIndexOf("/") + 1);
            }
            if (Applications.ContainsKey(CurrentPageFolder))
            {
                return (AuthApplication)Applications[CurrentPageFolder];
            } else
            {
                return null;
            }
        }

        public static Guid? ConvertStringToGuid(string inputString)
        {
            var ID = (Guid?)null;
            try
            {
                ID = Guid.Parse(inputString);
            } catch (Exception ex)
            {
                
            }
            return ID;
        }

        public static string ReplaceDefaultValue(string inputString, string defaultString)
        {
            return (inputString == defaultString) ? "" : inputString;
        }

        public static string CurApplicationpath(string AppID)
        {
            switch(AppID)
            {
                case "OperationKPI":
                    return "~/Application/CCG/OperationKPI/";
                case "ConCreteKPI":
                    return "~/Application/CCG/ConcreteKPI/";
                case "S2NKPI":
                    return "~/Application/CCG/S2NKPI/";
                case "PMSCKPI":
                    return "~/Application/CCG/PMSCKPI/";
                case "ShirleyKPI":
                    return "~/Application/SCC/ShirleyKPI/";
                case "AtkinsonKPI":
                    return "~/Application/ATK/AtkinsonKPI/";
                case "IPMWebAccess":
                    return "~/Application/CCG/IPMWebAccess/";
                case "Estimating":
                    return "~/Application/CCG/Estimating/";
                case "Marketing":
                    return "~/Application/CCG/Marketing/";
                case "Schedule":
                    return "89D74301-7207-4CD9-8D59-74B9AAC170EE/";
                default:
                    return "";
               }
        }

        public static string CurApplicationPath2(string AppID)
        {
            switch (AppID)
            {
                case "ATK":
                    return "~/Application/ATK/AtkinsonKPI/";
                case "CCC":
                    return "~/Application/CCG/ConcreteKPI/";
                case "CCG":
                    return "~/Application/CCG/OperationKPI/";
                case "S2N":
                    return "~/Application/CCG/S2NKPI/";
                case "SCH":
                    return "89d74301-7207-4cd9-8d59-74b9aac170ee/";
                default:
                    return "";
            }
        }


        public static Int16 CurApplicationKeyType(string AppID)
        {
            switch (AppID)
            {
                case "OperationKPI":
                case "Marketing":
                    return 1010;
                case "ConcreteKPI":
                    return 1020;
                case "S2NKPI":
                    return 1030;
                case "PMSCKPI":
                    return 1040;
                case "ShirleyKPI":
                    return 5010;
                case "AtkinsonKPI":
                    return 6010;
                case "Estimating":
                    return 8000;
                case "Schedule":
                    return 8060;
                default:
                    return 0;
            }
        }

        public static Int16 CurApplicationKeyType2(string AppID)
        {
            switch(AppID)
            {
                case "ATK":
                    return 6010;
                case "CCC":
                    return 1020;
                case "CCG":
                    return 1010;
                case "S2N":
                    return 1030;
                case "SCH":
                    return 8060;
                default:
                    return 0;
            }
        }

        public static string Build_ApplicationPath(bool Opt)
        {
            string TempString = (System.Web.HttpContext.Current.Request.ServerVariables["HTTPS"].ToString() == "on") ? "https://" : "http://";
            TempString = TempString + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"].ToString();
            TempString = TempString + ((System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"].ToString() == "80") ? "" : (":" + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"].ToString()));
            TempString = TempString + System.Web.HttpContext.Current.Request.ServerVariables["URL"].ToString();
            if (Opt)
            {
                TempString = TempString + ((System.Web.HttpContext.Current.Request.ServerVariables["QUERY_STRING"].ToString() == "") ? "" : ("?" + System.Web.HttpContext.Current.Request.ServerVariables["QUERY_STRING"].ToString()));
            }
            return TempString;
        }

        public static Int32 SSO_Check_LocalIP(string IPAddress)
        {
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISystem.LocalIPAddress_Check";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);
            oSqlConnection.Open();
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(oSqlCommand);
            oSqlCommand.Parameters["@IPAddress"].Value = IPAddress;
            oSqlCommand.ExecuteNonQuery();
            Int32 RtnValue = (int)oSqlCommand.Parameters["@RETURN_VALUE"].Value;
            oSqlCommand.Dispose();
            oSqlConnection.Close();

            return 1;
        }

        public static Guid SSO_Create_Ticket(string SessionID, string ApplicationURL, string Key = "")
        {
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISystem.SignOnLog_Insert";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);
            oSqlConnection.Open();
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(oSqlCommand);
            oSqlCommand.Parameters["@SessionID"].Value = SessionID;
            oSqlCommand.Parameters["@Application"].Value = ApplicationURL;
            if (Key != "")
            {
                oSqlCommand.Parameters["@Key"].Value = Key;
            }
            oSqlCommand.ExecuteNonQuery();
            Guid RtnID = (Guid)oSqlCommand.Parameters["@ReturnID"].Value;
            oSqlCommand.Dispose();
            oSqlConnection.Close();
            return RtnID;
        }

        public static string SSO_Delete_Ticket(string ReturnID)
        {
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISystem.SignOnLog_Delete";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);
            oSqlConnection.Open();
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(oSqlCommand);
            oSqlCommand.Parameters["@ID"].Value = new Guid(ReturnID);
            oSqlCommand.ExecuteNonQuery();
            string RtnKey = (string)ReplaceDBNull(oSqlCommand.Parameters["@Key"].Value, "");
            oSqlCommand.Dispose();
            oSqlConnection.Close();
            return RtnKey;
        }
        

    }
}