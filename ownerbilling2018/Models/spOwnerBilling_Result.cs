//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ownerbilling2018.Models
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Globalization;
    using System.ComponentModel.DataAnnotations;

    [AttributeUsage(AttributeTargets.Property)]
    public class CurrencyAttribute : Attribute
    {
    }

    [ModelBinder(typeof(TestModelBinder))]
    public partial class spOwnerBilling_Result
    {
        public Nullable<long> RowNum { get; set; }
        public string MMRegionGroup { get; set; }
        public Nullable<decimal> QG_A02 { get; set; }
        public string ENDescription { get; set; }
        public string MMRegion { get; set; }
        public Nullable<bool> OBIsCurrent { get; set; }
        public string jobdescription { get; set; }
        public string MMMCU { get; set; }
        public short MMKeyType { get; set; }
        public Nullable<short> OBSeqNoId { get; set; }
        public Nullable<short> OBApplicationNo { get; set; }
        public Nullable<System.DateTime> OBRequisitionDate { get; set; }
        public Nullable<byte> OBStatus { get; set; }
        public string OBStatusDescription { get; set; }
        public Nullable<System.DateTime> OBSubmitDate { get; set; }
        public Nullable<System.DateTime> OBApproveDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OBTotalEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OBTotalFinal { get; set; }
        public Nullable<System.DateTime> KDOwnerPaymentReceivedDate { get; set; }
        public Nullable<System.DateTime> G4DueDate { get; set; }
        public string OBUserName { get; set; }
        public Nullable<System.DateTime> OBDateAdd { get; set; }
        public Nullable<System.DateTime> OBDateChange { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBSubWorkEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBClarkWorkEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBOwnerRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBSubRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBSubWorkFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBClarkWorkFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBOwnerRetentionFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OBSubRetentionFinal { get; set; }
        public string OBRemark { get; set; }
        public string G4PTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OPSubWorkEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXSubWorkEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXSubWorkFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OPClarkWorkEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXClarkWorkEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXClarkWorkFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OPBillingEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OBBillingEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXBillingEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OBBillingFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXBillingFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OPOwnerRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXOwnerRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXOwnerRetentionFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<decimal> OPSubRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXSubRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXSubRetentionFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OPRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OBRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXRetentionEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OBRetentionFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXRetentionFinal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXTotalEstimate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}", ConvertEmptyStringToNull = true)]
        [Currency]
        public Nullable<double> OXTotalFinal { get; set; }
    }

    public class TestModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            object bindingObject = base.BindModel(controllerContext,
                bindingContext);

            foreach (System.Reflection.PropertyInfo propInfo
                in bindingObject.GetType().GetProperties())
            {
                object[] attributes =
                    propInfo.GetCustomAttributes(typeof(CurrencyAttribute), false);

                foreach (object attribute in attributes)
                {
                    CurrencyAttribute currAtt = attribute as CurrencyAttribute;

                    if (currAtt != null)
                    {
                        try
                        {


                            bindingContext.ModelState[propInfo.Name].Errors.Clear();

                            string attempted = bindingContext.ValueProvider.GetValue(propInfo.Name).AttemptedValue;

                            propInfo.SetValue(
                                bindingObject,
                                decimal.Parse(attempted, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat),
                                null);
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                }
            }

            return bindingObject;
        }
    }
}
