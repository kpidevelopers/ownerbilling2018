﻿function hideOverlayMask() {
    $('#masterOverlayMask').fadeOut('normal');
    $('#searchcompany').fadeOut('normal');
    $('#searchcompany').children().fadeOut('normal');


    
}

function headersubmit() {
 
    $('#headerform').submit();
}

function showOverlayMask() {
    

    var maskHeight = $(document).height(), maskWidth = $(window).width();
    $('#masterOverlayMask').css({ 'height': maskHeight, 'width': maskWidth });
    $('#masterOverlayMask').css({ 'display': 'block' });
    $('#masterOverlayMask').fadeTo('normal', 0.4);
    
    $('#searchcompany').fadeIn('normal');
    $('#searchcompany').children().fadeIn('normal');

//    focusObject.focus();
}

function checkFormView(typeA) {
    
    
    if (typeA == 'D') return confirm('Are you sure you want to delete this Requisition?');
    if (typeA == 'O00') return confirm('Are you sure you want to reopen this Requisition?');
    if (typeA == 'O10') return confirm('Are you sure you want to reopen this Requisition?');
    if (typeA == 'U30') return confirm('Are you sure you want to skip this Requisition?');


    if ((typeA == 'U' || typeA == 'U10') && verifyForms('', 'OBApplicationNo')) return false;

    if (typeA == 'U' && verifyForms('', 'OBSubWorkEstimate')) return false;
    if (typeA == 'U' && verifyForms('', 'OBClarkWorkEstimate')) return false;
    if (typeA == 'U' && verifyForms('', 'OBOwnerRetentionEstimate')) return false;
    if (typeA == 'U' && verifyForms('', 'OBSubRetentionEstimate')) return false;

    if (typeA == 'U10' && verifyForms2('', 'OBSubWorkEstimate', 'XXSubWorkEstimate')) return false;
    if (typeA == 'U10' && verifyForms2('', 'OBClarkWorkEstimate', 'XXClarkWorkEstimate')) return false;
    if (typeA == 'U10' && verifyForms2('', 'OBOwnerRetentionEstimate', 'XXOwnerRetentionEstimate')) return false;
    if (typeA == 'U10' && verifyForms2('', 'OBSubRetentionEstimate', 'XXSubRetentionEstimate')) return false;

    if ((typeA == 'U' || typeA == 'U10') && verifyForms('', 'OBSubmitDate')) return false;
    if ((typeA == 'U' || typeA == 'U10') && verifyForms('', 'OBApproveDate')) return false;
    if ((typeA == 'U' || typeA == 'U10') && verifyForms('', 'OBSubWorkFinal')) return false;
    if ((typeA == 'U' || typeA == 'U10') && verifyForms('', 'OBClarkWorkFinal')) return false;
    if ((typeA == 'U' || typeA == 'U10') && verifyForms('', 'OBOwnerRetentionFinal')) return false;
    if ((typeA == 'U' || typeA == 'U10') && verifyForms('', 'OBSubRetentionFinal')) return false;
    
    if (typeA == 'U20' && verifyForms2('', 'OBSubmitDate', 'XXSubmitDate')) return false;
    if (typeA == 'U20' && verifyForms2('', 'OBApproveDate', 'XXApproveDate')) return false;
    if (typeA == 'U20' && verifyForms2('', 'OBSubWorkFinal', 'XXSubWorkFinal')) return false;
    if (typeA == 'U20' && verifyForms2('', 'OBClarkWorkFinal', 'XXClarkWorkFinal')) return false;
    if (typeA == 'U20' && verifyForms2('', 'OBOwnerRetentionFinal', 'XXOwnerRetentionFinal')) return false;
    if (typeA == 'U20' && verifyForms2('', 'OBSubRetentionFinal', 'XXSubRetentionFinal')) return false;

    if (typeA == 'U10') return confirm('Are you sure you want to submit this Requisition?');
    if (typeA == 'U20') return confirm('Are you sure you want to submit this Requisition?');
    if (typeA == 'U30') return confirm('Are you sure you want to skip this Requisition?');
    
    return true;
}

function verifyFields(obj, objID, showError, objRef, defaultFocus) {
 
    if (arguments.length <= 2) showError = false;
    
    switch (objID) {
        case 'OBRequisitionDate':
            return dataValidation(obj, 'Date', 'Requisition Date', true, showError);

        case 'OBApplicationNo':
            return dataValidation(obj, 'Number', 'Application No', true, showError, '', 0, 5000, false, 0);

        case 'OBSubmitDate':
            return dataValidation(obj, 'Date', 'Submit Date', false, showError);
        case 'XXSubmitDate':
            return dataValidation(obj, 'Date', 'Submit Date', true, showError);

        case 'OBApproveDate':
            return dataValidation(obj, 'Date', 'Approve Date', false, showError);
        case 'XXApproveDate':
            return dataValidation(obj, 'Date', 'Approve Date', true, showError);

        case 'OBSubWorkEstimate':
        
            return dataValidation(obj, 'Currency', 'Estimate Subcontractor Work', false, showError, '', null, null, true, 2);
        case 'XXSubWorkEstimate':
    
            return dataValidation(obj, 'Currency', 'Estimate Subcontractor Work', true, showError, '', null, null, true, 2);

        case 'OBClarkWorkEstimate':
            return dataValidation(obj, 'Currency', 'Estimate Clark Work', false, showError, '', null, null, true, 2);
        case 'XXClarkWorkEstimate':
            return dataValidation(obj, 'Currency', 'Estimate Clark Work', true, showError, '', null, null, true, 2);

        case 'OBOwnerRetentionEstimate':
            return dataValidation(obj, 'Currency', 'Estimate Owner Retention Amount', false, showError, '', null, null, true, 2);
        case 'XXOwnerRetentionEstimate':
            return dataValidation(obj, 'Currency', 'Estimate Owner Retention Amount', true, showError, '', null, null, true, 2);

        case 'OBSubRetentionEstimate':
            return dataValidation(obj, 'Currency', 'Estimate Sub Work Retention Amount', false, showError, '', null, null, true, 2);
        case 'XXSubRetentionEstimate':
            return dataValidation(obj, 'Currency', 'Estimate Sub Work Retention Amount', true, showError, '', null, null, true, 2);

        case 'OBSubWorkFinal':
            return dataValidation(obj, 'Currency', 'Final Subcontractor Work', false, showError, '', null, null, true, 2);
        case 'XXSubWorkFinal':
            return dataValidation(obj, 'Currency', 'Final Subcontractor Work', true, showError, '', null, null, true, 2);

        case 'OBClarkWorkFinal':
            return dataValidation(obj, 'Currency', 'Final Clark Work', false, showError, '', null, null, true, 2);
        case 'XXClarkWorkFinal':
            return dataValidation(obj, 'Currency', 'Final Clark Work', true, showError, '', null, null, true, 2);

        case 'OBOwnerRetentionFinal':
            return dataValidation(obj, 'Currency', 'Final Owner Retention Amount', false, showError, '', null, null, true, 2);
        case 'XXOwnerRetentionFinal':
            return dataValidation(obj, 'Currency', 'Final Owner Retention Amount', true, showError, '', null, null, true, 2);

        case 'OBSubRetentionFinal':
            return dataValidation(obj, 'Currency', 'Final Sub Work Retention Amount', false, showError, '', null, null, true, 2);
        case 'XXSubRetentionFinal':
            return dataValidation(obj, 'Currency', 'Final Sub Work Retention Amount', true, showError, '', null, null, true, 2);

        case 'OBRemark':
            return dataValidation(obj, 'Text', 'Remark', false, showError);

        default:
            alert('Validation Field ' + objID + ' is not define!');
            return false;
    }
}

function dataValidation(dataElement, dataElementType, dataElementName, isRequired, showError, valueDefault, valueLow, valueHign, showSeparator, digitAfterDecimal) {
    
    if (arguments.length <= 5) valueDefault = '';
    if (arguments.length <= 6) valueLow = null;
    if (arguments.length <= 7) valueHign = null;
    if (arguments.length <= 8) showSeparator = true;
    if (arguments.length <= 9) digitAfterDecimal = 0;

    switch (dataElementType.toUpperCase()) {
        case 'TEXT':
            if (isRequired) return kpiValidationRequired(dataElement, dataElementName, showError);
            return kpiValidationTrimText(dataElement);

        case 'NUMBER':
            return kpiValidationNumber(dataElement, dataElementName, !isRequired, valueDefault, valueLow, valueHign, false, showSeparator, digitAfterDecimal, showError);

        case 'CURRENCY':
            return kpiValidationNumber(dataElement, dataElementName, !isRequired, valueDefault, valueLow, valueHign, true, showSeparator, digitAfterDecimal, showError);

        case 'DATE':
            return kpiValidationDate(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'SELECT':
            if (isRequired) return kpiValidationSelected(dataElement, dataElementName, showError);
            return true;

        case 'EMAIL':
            return kpiValidationEMailAddress(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'EMAILS':
            return kpiValidationEMailAddresses(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'PHONE':
            return kpiValidationPhoneNo(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'RADIOBUTTON':
            return kpiValidationRadioButton(dataElement, dataElementName, valueDefault);

        default:
        
            alert('Data Element Type ' + dataElementType + ' is not define!');
            return false;
    }
}

function kpiTrim(str) {
    var whitespace = new String(" \t\n\r"), s = new String(str), i;
    i = 0;
    while (i < s.length && whitespace.indexOf(s.charAt(i)) != -1) i++;
    s = s.substring(i, s.length);
    i = s.length - 1;
    while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1) i--;
    s = s.substring(0, i + 1);
    return s;
}

function kpiGetNumber(inputValue, allowEmpty, defaultValue) {

    var isNeg = false;

    defaultValue = String(defaultValue);
    var rtnValue = kpiTrim(inputValue);
    if (!allowEmpty && rtnValue == '') return '*EmptyNotAllowed*';
    if (rtnValue == '') return defaultValue;
    if (rtnValue.toUpperCase() == defaultValue.toUpperCase()) return defaultValue;


    if (rtnValue.charAt(0) == '(' && rtnValue.charAt(rtnValue.length - 1) == ')') {
        rtnValue = rtnValue.substring(1, rtnValue.length - 1);
        isNeg = true;
    }

    if (rtnValue.charAt(0) == '-' && !isNeg) {
        rtnValue = rtnValue.substring(1, rtnValue.length);
        isNeg = true;
    }

    if (rtnValue.charAt(0) == '$') { rtnValue = rtnValue.substring(1, rtnValue.length); }

    if (rtnValue.indexOf('-') != -1) return NaN;

    for (var i = rtnValue.indexOf(','); i != -1; i = rtnValue.indexOf(',')) { rtnValue = rtnValue.substring(0, i) + rtnValue.substring(i + 1, rtnValue.length); }

    rtnValue = parseFloat(rtnValue);

    if (isNaN(rtnValue)) return NaN;

    return rtnValue * (isNeg ? -1 : 1);
}

function kpiFormatNumber(inputValue, showCurrency, showGroup, showDecimal) {

    var isNeg = false;

    var rtnValue = inputValue;
    if (rtnValue < 0) {
        rtnValue = -1 * rtnValue;
        isNeg = true;
    }

    rtnValue = String(Math.round(rtnValue * Math.pow(10, showDecimal)));

    if (rtnValue.length <= showDecimal) { rtnValue = String(Math.pow(10, showDecimal - rtnValue.length + 1)).slice(1) + rtnValue; }

    if (showDecimal != 0) { rtnValue = rtnValue.substring(0, rtnValue.length - showDecimal) + '.' + rtnValue.substring(rtnValue.length - showDecimal, rtnValue.length); }

    if (showGroup) {
        for (var i = rtnValue.length - (showDecimal == 0 ? 3 : showDecimal + 4); i >= 1; i -= 3) { rtnValue = rtnValue.substring(0, i) + ',' + rtnValue.substring(i, rtnValue.length); }
    }

    if (showCurrency) { rtnValue = '$' + rtnValue; }

    if (isNeg) { rtnValue = (showCurrency ? '(' : '-') + rtnValue + (showCurrency ? ')' : '') };

    return rtnValue;
}

function kpiValidationNumber(objectRef, objectName, allowEmpty, defaultValue, rangeLow, rangeHigh, showCurrency, showGroup, showDecimal, showError) {
 
    //var rtnValue = kpiGetNumber(objectRef.value, allowEmpty, defaultValue);
    var rtnValue = kpiGetNumber(objectRef.val(), allowEmpty, defaultValue);
   
    if (rtnValue == '*EmptyNotAllowed*') {

        if (showError) {
            alert('Please enter ' + objectName + '!');
            objectRef.focus();
        }
        return false;
    }

    if (allowEmpty && rtnValue.toString == defaultValue.toString) {
       
        objectRef.val(rtnValue);
        return true;
    }

    if (isNaN(rtnValue)) {
      
        if (showError) {
            alert('You have entered an invalid number for ' + objectName + '.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }

    if (rangeLow != null && rangeLow > rtnValue) {
        if (showError) {
            alert(objectName + ' should be equal to or greater than ' + rangeLow + '.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }

    if (rangeHigh != null && rangeHigh < rtnValue) {
        if (showError) {
            alert(objectName + ' should be equal to or less than ' + rangeHigh + '.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }
 
    objectRef.val(kpiFormatNumber(rtnValue, showCurrency, showGroup, showDecimal));
    //objectRef.value = kpiFormatNumber(rtnValue, showCurrency, showGroup, showDecimal);
   
    return true;
}

function kpiValidationRequired(objectRef, objectName, showError) {
    objectRef.val(kpiTrim(objectRef.val()));
    if (objectRef.val() == '') {
        if (showError) {
            alert('Please enter ' + objectName + '!');
            objectRef.focus();
        }
        return false;
    }

    return true;
}

function kpiTrimDescription(objectRef) {
    objectRef.val(kpiTrim(objectRef.val()));
    return;
}

function kpiValidationSelected(objectRef, objectName, showError) {
    if (arguments.length <= 2) showError = true;

    if (objectRef.options[objectRef.selectedIndex].value == '') {
        if (showError) {
            alert('Please specify ' + objectName + '!');
            objectRef.focus();
        }
        return false;
    }
    return true;
}

function kpiValidationDate(objectRef, objectName, allowEmpty, defaultValue, showError) {
    var str, i, j, iY, iM, iD;
    objectRef.val(kpiTrim(objectRef.val()));

    if (allowEmpty && objectRef.val() == '') {
    objectRef.val(defaultValue);
        return true;
    }

    if (allowEmpty && objectRef.val() == defaultValue) return true;

    if (!allowEmpty && objectRef.val() == '') {
        if (showError) {
            alert('Please enter ' + objectName + '!');
            objectRef.focus();
        }
        return false;
    }

    str = objectRef.val();
    i = 0;
    while (str.charAt(i) != '/' && str.charAt(i) != '-' && i < str.length) i++;
    j = i + 1;
    while (str.charAt(j) != '/' && str.charAt(j) != '-' && j < str.length) j++;
    if (i >= 1 && i <= 2 && j - i >= 2 && j - i <= 3 && str.length - j == 3 || str.length - j == 5) {
        iY = parseInt(str.substring(j + 1, str.length), 10);
        iM = parseInt(str.substring(0, i), 10);
        iD = parseInt(str.substring(i + 1, j), 10);
        if (!isNaN(iY) && iY >= 0 && iY <= 29) iY += 2000;
        if (!isNaN(iY) && iY >= 30 && iY <= 99) iY += 1900;
        if (!isNaN(iY) && iY >= 1900 && iY <= 2099 && !isNaN(iM) && iM >= 1 && iM <= 12 && iD >= 1 && iD <= 31) {
            if (iM == 1 || iM == 3 || iM == 5 || iM == 7 || iM == 8 || iM == 10 || iM == 12 || iD <= 30) {
                if (iM != 2 || iD <= 28 + (iY % 4 == 0 ? 1 : 0)) {
                    if (iM < 10) iM = '0' + iM;
                    if (iD < 10) iD = '0' + iD;
                    objectRef.val(iM + '/' + iD + '/' + iY);
                    return true;
                }
            }
        }
    }
    if (showError) {
        alert('You have entered an invalid date for ' + objectName + '.\n Correct Date Format is "12/31/2000"');
        objectRef.select();
        objectRef.focus();
    }
    return false;
}

function kpiValidationRadioButton(objectRef, objectName, defaultFocus) {
    for (var i = 0; i < objectRef.length; i++) { if (objectRef[i].checked) return true; }

    alert('Please pick ' + objectName + '!');
    objectRef[defaultFocus].focus();
    return false;
}

function kpiValidationEMailAddresses(objectRef, objectName, allowEmpty, defaultValue, showError) {
    var rtnValue = kpiTrim(objectRef.val()).replace(/\;/g, ',').replace(/\s/g, '');//replace ";" with "," and remove space

    objectRef.val(rtnValue);

    if (!allowEmpty && rtnValue == '') {
        if (showError) {
            alert('Please enter ' + objectName + '!');
            objectRef.focus();
        }
        return false;
    }

    if (allowEmpty && (rtnValue == '' || rtnValue == defaultValue)) {
        objectRef.val(defaultValue);
        return true;
    }

    if (rtnValue.indexOf(',,') != -1) {
        if (showError) {
            alert('Please remove extra seperators!');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }

    var addresses = rtnValue.split(',');
    var j = 0; error = false;

    for (var i in addresses) {
        if (addresses[i] != '') {
            var regexpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            //var regexpEmail = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (regexpEmail.test(addresses[i])) { j++; continue; }
            error = true;
        }
    }

    if (error) {
        if (showError) {
            alert('You have entered an invalid email address format for ' + objectName + '.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }

    if (j == 0) {
        if (showError) {
            alert('You entered an invalid email address format for ' + objectName + '.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }

    return true;
}


function kpiValidationEMailAddress(objectRef, objectName, allowEmpty, defaultValue, showError) {
    var rtnValue = kpiTrim(objectRef.val());
    objectRef.val(rtnValue);

    //alert(allowEmpty);
    //alert(defaultValue);
    //alert(showError);

    if (!allowEmpty && rtnValue == '') {
        if (showError) {
            alert('Please enter ' + objectName + '!');
            objectRef.focus();
        }
        return false;
    }

    if (allowEmpty && (rtnValue == '' || rtnValue == defaultValue)) {
        objectRef.val(defaultValue);
        return true;
    }

    //var regexpEmail = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var regexpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexpEmail.test(rtnValue)) return true;

    if (showError) {
        alert('You have entered an invalid email address format for ' + objectName + '.');
        objectRef.select();
        objectRef.focus();
    }
    return false;
}

function kpiValidationPhoneNo(objectRef, objectName, allowEmpty, defaultValue, showError) {
    var rtnValue = kpiTrim(objectRef.val());
    var ext = '', extPosition = -1;

    objectRef.val(rtnValue);

    if (!allowEmpty && rtnValue == '') {
        if (showError) {
            alert('Please enter ' + objectName + '!');
            objectRef.focus();
        }
        return false;
    }

    if (allowEmpty && (rtnValue == '' || rtnValue == defaultValue)) {
        objectRef.val(defaultValue);
        return true;
    }

    extPosition = rtnValue.toLowerCase().indexOf("ext");
    if (extPosition == -1) extPosition = rtnValue.toLowerCase().indexOf("x");
    if (extPosition >= 0) { ext = rtnValue.substr(extPosition); rtnValue = rtnValue.substr(0, extPosition); }

    rtnValue = rtnValue.replace(/[-() ]/g, '');
    rtnValue = parseFloat(rtnValue);

    if (isNaN(rtnValue)) {
        if (showError) {
            alert('You have entered an invalid format for ' + objectName + '.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }

    if (rtnValue < 1 || rtnValue > 9999999999) {
        if (showError) {
            alert('You have entered an invalid format for ' + objectName + '.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }

    rtnValue = String(rtnValue + 10000000000);
    rtnValue = '(' + rtnValue.substring(1, 4) + ') ' + rtnValue.substring(4, 7) + '-' + rtnValue.substring(7, 11);
    objectRef.val(rtnValue + (ext == "", "", " " + ext));
    return true;
}

function kpiValidationOrder(objectRef1, objectName1, objectRef2, objectName2, showError, option) {//option 1: value A >= B
    //       2: value A >  B
    //       3: value A <= B
    //       4: value A <  B
    //      11: date  A >= B
    //      12: date  A >  B
    //      13: date  A <= B
    //      14: date  A <  B

    var d1 = objectRef1.value, d2 = objectRef2.value;

    if (d1 == '' || d2 == '') return true;

    if (option <= 10) {
        d1 = kpiGetNumber(d1, false, 0);
        d2 = kpiGetNumber(d2, false, 0);
    }

    if (option > 10) {
        d1 = Date.parse(d1);
        d2 = Date.parse(d2);
        option -= 10;
    }

    if (option == 1 && d1 < d2) {
        alert(objectName2 + ' has to be equal to or less than ' + objectName1 + '.');
        objectRef1.select();
        objectRef1.focus();
        return false;
    }
    if (option == 2 && d1 <= d2) {
        alert(objectName2 + ' has to be less than ' + objectName1 + '.');
        objectRef1.select();
        objectRef1.focus();
        return false;
    }
    if (option == 3 && d1 > d2) {
        alert(objectName2 + ' has to be equal to or greater than ' + objectName1 + '.');
        objectRef1.select();
        objectRef1.focus();
        return false;
    }
    if (option == 4 && d1 >= d2) {
        alert(objectName2 + ' has to be greater than ' + objectName1 + '.');
        objectRef1.select();
        objectRef1.focus();
        return false;
    }
    return true;
}

function kpiValidationDateOrder(objectRef1, objectName1, objectRef2, objectName2, showError, option) {
    if (arguments.length <= 5) option = 13;
    return kpiValidationOrder(objectRef1, objectName1, objectRef2, objectName2, showError, option);

    // if(objectRef1.value == '') return true;
    // if(objectRef2.value == '') return true;

    // var date1 = Date.parse(objectRef1.value);
    // var date2 = Date.parse(objectRef2.value);

    // if(option == 1 && date1 < date2)
    // {alert(objectName2 + ' has to be equal to or less than ' + objectName1 + '.');
    //  objectRef1.select();
    //  objectRef1.focus();
    //  return false;
    // }
    // if(option == 2 && date1 <= date2)
    // {alert(objectName2 + ' has to be less than ' + objectName1 + '.');
    //  objectRef1.select();
    //  objectRef1.focus();
    //  return false;
    // }
    // if(option == 3 && date1 > date2)
    // {alert(objectName2 + ' has to be equal to or greater than ' + objectName1 + '.');
    //  objectRef1.select();
    //  objectRef1.focus();
    //  return false;
    // }
    // if(option == 4 && date1 >= date2)
    // {alert(objectName2 + ' has to be greater than ' + objectName1 + '.');
    //  objectRef1.select();
    //  objectRef1.focus();
    //  return false;
    // }
    // return true;
}

function kpiValidationTextBoxSize(objectRef, objectName, maxSize, showError) {
objectRef.val(kpiTrim(objectRef.val()));
    if (objectRef.val().length > maxSize) {
        if (showError) {
            alert(objectName + ' field has more than ' + maxSize + ' characters.');
            objectRef.select();
            objectRef.focus();
        }
        return false;
    }
    return true
}

function kpiValidationTrimText(objectRef) {
objectRef.val(kpiTrim(objectRef.val()));
    return true;
}

function verifyForm(objectPrefix, objectName, defaultFocus) {
    if (arguments.length <= 2) return !verifyField(document.getElementById(objectPrefix + objectName), true);
    return !verifyField(document.getElementsByName(objectPrefix + objectName), true, defaultFocus);
}



function verifyForms(objPrefix, objID, objRefID, defaultFocus) {
    //var obj = document.getElementById(objPrefix + objID);
    
    var obj = $('#' + objID);
    if (arguments.length <= 2) return !verifyFields(obj, objID, true);

    if (arguments.length <= 3) {
        var objRef = document.getElementById(objPrefix + objRefID);
        return !verifyFields(obj, objID + '-' + objRefID, true, objRef);
    }

    //obj = document.getElementsByName(objPrefix + objID)
    var obj = $('#' + objID);
    return !verifyFields(obj, objID, true, null, defaultFocus);
}

function verifyForms2(objPrefix, objID, objIDRef) {
    //var obj = document.getElementById(objPrefix + objID);
    var obj = $('#' + objID);
    return !verifyFields(obj, objIDRef, true);
}




function kpiTrimObject(objPrefix, objID) {
    var obj = document.getElementById(objPrefix + objID);
    return kpiTrim(obj.value);
}
//-------------------------------------------------------------------------------------------------
function dataValidation(dataElement, dataElementType, dataElementName, isRequired, showError, valueDefault, valueLow, valueHign, showSeparator, digitAfterDecimal) {
   
    if (arguments.length <= 5) valueDefault = '';
    if (arguments.length <= 6) valueLow = null;
    if (arguments.length <= 7) valueHign = null;
    if (arguments.length <= 8) showSeparator = true;
    if (arguments.length <= 9) digitAfterDecimal = 0;


    switch (dataElementType.toUpperCase()) {
        case 'TEXT':
            if (isRequired) return kpiValidationRequired(dataElement, dataElementName, showError);
            return kpiValidationTrimText(dataElement);

        case 'NUMBER':
            return kpiValidationNumber(dataElement, dataElementName, !isRequired, valueDefault, valueLow, valueHign, false, showSeparator, digitAfterDecimal, showError);

        case 'CURRENCY':
    
            return kpiValidationNumber(dataElement, dataElementName, !isRequired, valueDefault, valueLow, valueHign, true, showSeparator, digitAfterDecimal, showError);

        case 'DATE':
            return kpiValidationDate(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'SELECT':
            if (isRequired) return kpiValidationSelected(dataElement, dataElementName, showError);
            return true;

        case 'EMAIL':
            return kpiValidationEMailAddress(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'EMAILS':
            return kpiValidationEMailAddresses(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'PHONE':
            return kpiValidationPhoneNo(dataElement, dataElementName, !isRequired, valueDefault, showError);

        case 'RADIOBUTTON':
            return kpiValidationRadioButton(dataElement, dataElementName, valueDefault);

        default:
            alert('Data Element Type ' + dataElementType + ' is not define!');
            return false;
    }
}
//-------------------------------------------------------------------------------------------------
function dataValidationServer(dataElement, dataElementType, dataElementName, isRequired, showError, defaultValue, valueLow, valueHign, showSeparator, digitAfterDecimal) {
 
    var dataElementID = dataElement.id.split('_').pop(), isCorrect;

    if (arguments.length <= 5) defaultValue = '';
    if (arguments.length <= 6) valueLow = null;
    if (arguments.length <= 7) valueHign = null;
    if (arguments.length <= 8) showSeparator = true;
    if (arguments.length <= 9) digitAfterDecimal = 0;

    isCorrect = dataValidation(dataElement, dataElementType, dataElementName, isRequired, showError, defaultValue, valueLow, valueHign, showSeparator, digitAfterDecimal);
    if (!isCorrect) return false;

    if (dataElement.value == dataElement.defaultValue || dataElement.value == '') return true;
    dataElement.defaultValue = dataElement.value;

    if (showError) return true;

    callbackValidation(dataElementID + '::' + dataElement.value, dataElement);
    return true;
}
//-------------------------------------------------------------------------------------------------
function dataValidationServer2(dataElement1, dataElement2, dataElementType1, dataElementType2, dataElementName1, dataElementName2, isRequired1, isRequired2, showError, focusObject, defaultValue1, defaultValue2, valueLow1, valueLow2, valueHign1, valueHign2, showSeparator1, showSeparator2, digitAfterDecimal1, digitAfterDecimal2) {
   
    var dataElementID = dataElement1.id.split('_').pop(), isCorrect;

    if (arguments.length <= 9) defaultValue1 = '';
    if (arguments.length <= 10) defaultValue2 = '';
    if (arguments.length <= 11) valueLow1 = null;
    if (arguments.length <= 12) valueLow2 = null;
    if (arguments.length <= 13) valueHign1 = null;
    if (arguments.length <= 14) valueHign2 = null;
    if (arguments.length <= 15) showSeparator1 = true;
    if (arguments.length <= 16) showSeparator2 = true;
    if (arguments.length <= 17) digitAfterDecimal1 = 0;
    if (arguments.length <= 18) digitAfterDecimal2 = 0;

    isCorrect = dataValidation(dataElement1, dataElementType1, dataElementName1, isRequired1, showError, defaultValue1, valueLow1, valueHign1, showSeparator1, digitAfterDecimal1);
    if (!isCorrect) return false;
    isCorrect = dataValidation(dataElement2, dataElementType2, dataElementName2, isRequired2, showError, defaultValue2, valueLow2, valueHign2, showSeparator2, digitAfterDecimal2);
    if (!isCorrect) return false;

    if (dataElement1.value == dataElement1.defaultValue && dataElement2.value == dataElement2.defaultValue || dataElement1.value == '' && dataElement2.value == '') return true;
    dataElement1.defaultValue = dataElement1.value;
    dataElement2.defaultValue = dataElement2.value;

    if (showError) return true;

    callbackValidation(dataElementID + '::' + dataElement1.value + '::' + dataElement2.value, focusObject);
    return true;
}
//-------------------------------------------------------------------------------------------------
